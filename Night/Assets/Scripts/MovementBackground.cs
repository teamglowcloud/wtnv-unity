using UnityEngine;
using System.Collections;

public class MovementBackground : MonoBehaviour {
	GameObject cam;
   	float offSet;
	[SerializeField] private float ratio = 0.1f;

	void Start () 
	{
		cam = GameObject.Find("Char");
		offSet = cam.transform.position.x / ratio;
	}
	
	// Update is called once per frame
	void Update () 
	{
		 Movement car = cam.GetComponent<Movement>();

			this.transform.position = new Vector3((cam.transform.position.x * ratio), this.transform.position.y, this.transform.position.z);

	}
	
}
