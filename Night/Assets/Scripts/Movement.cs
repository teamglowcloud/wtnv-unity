using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	public Vector3 worldPos;
	public bool moving;
	public float go;
	public bool interacting;
	public bool AD;
	GameObject cont;
	[SerializeField] float speed;
	// Use this for initialization
	void Start () {
	currentFrame = 0;
	StartCoroutine (Animate ());
	secToWait = 1/FPS;
	go = this.transform.position.x;
	cont = GameObject.Find ("Controller(Clone)");
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		Debug.Log (AD);
		Pause con = cont.GetComponent<Pause>();
		worldPos = (this.camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,Input.mousePosition.y,camera.nearClipPlane)));
		if (AD == false && con.paused == false)
		{
		//gets the mouse position in 3D space
		
		if (interacting == false)
		{
		
		if (Input.GetButton ("Fire1") )
		{
			//sets the mouse 3D position as the position the character will move too
			go = worldPos.x;
		
		}
		if (this.transform.position.x != go)
		{
			//moves the character to the position when clicked
			this.transform.position = new Vector3(Mathf.Lerp (this.transform.position.x, go, 0.005f + speed * 0.001f), this.transform.position.y, this.transform.position.z);
			moving = true;
			
		}
		}
		this.rigidbody.velocity = new Vector3(0,0,0);
		}
		else if (con.paused == false)
		{
		this.transform.position = new Vector3(this.transform.position.x + Input.GetAxis("Horizontal")  * speed * 0.11f, this.transform.position.y, this.transform.position.z);
		go = this.transform.position.x + Input.GetAxis ("Horizontal") * 10; 
		}
		
	}
	
	public float FPS;
	float secToWait;
	bool loop = true;
	public GameObject car;
	public Texture[] framesR;
	public Texture[] framesL;
	public Texture[] idleR;
	public Texture[] idleL;
	int currentFrame;
	//Animation of the character
	IEnumerator Animate()
	{
		bool stop = false;
		if (currentFrame >= framesL.Length || currentFrame >= framesR.Length)
		{
			if (loop == false)
			{
				stop = true;
			}
			else
			{
				currentFrame = 0;
			}
		}
		
		yield return new WaitForSeconds(secToWait);
		if ((AD == true  && Input.GetButton ("Horizontal") == true && go > this.transform.position.x) || (AD == false) && go > this.transform.position.x + 5)
		{
		car.renderer.material.mainTexture = framesR[currentFrame];
		}
		else if ((AD == true  && Input.GetButton ("Horizontal") == true && go < this.transform.position.x) || (AD == false) && go < this.transform.position.x -5)
		{
		car.renderer.material.mainTexture = framesL[currentFrame];	
		}
		else if ((Input.GetButtonUp ("Horizontal") == false && go > this.transform.position.x) || (AD == false && this.transform.position.x < go))
		{
		car.renderer.material.mainTexture = idleR[currentFrame];	
		}
		else if ((Input.GetButtonUp ("Horizontal") == false && go < this.transform.position.x) || (AD == false && this.transform.position.x > go))
		{
		car.renderer.material.mainTexture = idleL[currentFrame];	
		}
		
		currentFrame =  currentFrame +1;
		if (stop == false)
		{
		StartCoroutine (Animate ());
		}
	}
	
}

