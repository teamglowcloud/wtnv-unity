﻿using UnityEngine;
using System.Collections;

public class AudioController : MonoBehaviour {
	[SerializeField] AudioClip[] clips;
	bool outro = false;
	bool josiep =false;
	bool radiop =false;
	bool minep =false;
	bool dinerp =false;
	bool yourp =false;
	bool forestp =false;
	GameObject mid;
	// Use this for initialization
	void Start () 
	{
		mid = GameObject.Find ("NPC-1-char");
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		Quest complete = this.GetComponent<Quest>();
	 	
		if (Application.loadedLevelName == "Intro")
			{
				
				Debug.Log ("woo");
				this.audio.clip	= clips[7];
				audio.PlayDelayed(0);
				audio.loop = false;
			
			}
		
		if (Application.loadedLevelName == "Josies" && josiep == false)
			{
				
				Debug.Log ("woo");
				this.audio.clip	= clips[0];
				audio.PlayDelayed(0);
				josiep = true;
				audio.loop = false;
			
			}
		
		if (Application.loadedLevelName == "Radio-Toilet" && radiop == false)
			{
				
				Debug.Log ("woo");
				this.audio.clip	= clips[1];
				audio.PlayDelayed(0);
				radiop = true;
				audio.loop = false;
			
			}
		if (Application.loadedLevelName == "Mine" && minep == false)
			{
				
				Debug.Log ("woo");
				this.audio.clip	= clips[2];
				audio.PlayDelayed(0);
				minep = true;
				audio.loop = false;
			}
	
		if (Application.loadedLevelName == "Diner" && dinerp == false)
			{
				
				Debug.Log ("woo");
				this.audio.clip	= clips[3];
				audio.PlayDelayed(0);
				dinerp = true;
				audio.loop = false;
			}
		if (Application.loadedLevelName == "Your-Trailer" && yourp == false)
			{
				
				Debug.Log ("woo");
				this.audio.clip	= clips[4];
				audio.PlayDelayed(0);
				yourp = true;
				audio.loop = false;
			
			}
		if (Application.loadedLevelName == "Forest" && forestp == false)
			{
				
				Debug.Log ("woo");
				this.audio.clip	= clips[5];
				audio.PlayDelayed(0);
				forestp = true;
				audio.loop = false;
			
			}

			Pyramid midscr = mid.GetComponent<Pyramid>();
			if (midscr.gameover == true && outro == false)
		{
				Debug.Log ("woo");
				this.audio.clip	= clips[6];
				audio.PlayDelayed(0);
				outro = true;
				audio.loop = false;		
		}
	
	}
}
