﻿using UnityEngine;
using System.Collections;

public class Quest : MonoBehaviour {
	

	void Awake () 
	{
		DontDestroyOnLoad (this.transform.gameObject);
	}
	// Use this for initialization
	void Start () 
	{
	
	}
	//persistance
	public bool josies = false;
	public bool radio = false;
	public bool your = false;
	public bool mine = false;
	public bool forest = false;
	public bool diner = false;
	public Texture2D josiesBub;
	public Texture2D radioBub;
	public Texture2D yourBub;
	public Texture2D mineBub;
	public Texture2D forestBub;
	public Texture2D dinerBub;
	
	public int numItems = 10;
	public int numInteracted = 0;
	//makes "quests" complete using a level check and booleans
	void Update () 
	{	
		GameObject NPCchar = GameObject.Find("NPC-Q-Char");
		NPC items = NPCchar.GetComponent<NPC>();
		numItems = items.numOfItems;
		GameObject NPCspeech = GameObject.Find("NPC-Q-Speech");
		Shader shader1 = Shader.Find("Transparent/Diffuse");
		if (josies == true)
				{
					
					
					NPCspeech.renderer.material.mainTexture = josiesBub;
					Animation2D anim = NPCspeech.GetComponent<Animation2D>();
					for (int i =0; i <= anim.frames.Length-1; i++)
					{
						anim.frames[i] = josiesBub;
					}
				}
		if (radio == true)
				{
					Debug.Log (diner);
					NPCspeech.renderer.material.mainTexture = radioBub;
					Animation2D anim = NPCspeech.GetComponent<Animation2D>();
					for (int i =0; i <= anim.frames.Length-1; i++)
					{
						anim.frames[i] = radioBub;
					}
					 
				}
		if (your== true)
				{
					Debug.Log (diner);
					NPCspeech.renderer.material.mainTexture = yourBub;
					Animation2D anim = NPCspeech.GetComponent<Animation2D>();
					for (int i =0; i <= anim.frames.Length-1; i++)
					{
						anim.frames[i] = yourBub;
					}
				}
		if (mine == true)
				{
					Debug.Log (diner);
					NPCspeech.renderer.material.mainTexture = mineBub;
					Animation2D anim = NPCspeech.GetComponent<Animation2D>();
					for (int i =0; i <= anim.frames.Length-1; i++)
					{
						anim.frames[i] = mineBub;
					}
				}
		if (forest == true)
				{
					Debug.Log (diner);
					NPCspeech.renderer.material.mainTexture = forestBub;
					Animation2D anim = NPCspeech.GetComponent<Animation2D>();
					for (int i =0; i <= anim.frames.Length-1; i++)
					{
						anim.frames[i] = forestBub;
					}
				}
		if (diner == true)
				{
					Debug.Log (diner);
					NPCspeech.renderer.material.mainTexture = dinerBub;
					Animation2D anim = NPCspeech.GetComponent<Animation2D>();
					for (int i =0; i <= anim.frames.Length-1; i++)
					{
						anim.frames[i] = dinerBub;
					}
				}
		if (numInteracted == numItems)
			{
			
			
			Debug.Log (numInteracted);
				string lvlname = Application.loadedLevelName;
			 	
				if (lvlname == "Josies")
					{
						josies = true;
					}
				else if (lvlname == "Radio-Toilet")
					{
						radio = true;
					}
				else if (lvlname == "Your-Trailer")
					{
						your = true;
					}
				else if (lvlname == "Mine")
					{
						mine = true;
					}
				else if (lvlname == "Forest")
					{
						forest = true;
					}
				else if (lvlname == "Diner")
					{
						diner = true;
					}
			
			}
	}
}
