using UnityEngine;
using System.Collections;

public class NPC : MonoBehaviour {
	public GameObject speech;
	bool mouseOver = false;
	bool clicked = false;
	Vector3 mePos;
	public float above;
	public bool quester;
	public int numOfItems;

	void Awake () {
		//if it is a puzzle giver.
	if (quester == true)
		{
			//GameObject controller = GameObject.Find("Controller(Clone)");
			//Quest questGive = controller.GetComponent<Quest>();
			//questGive.numItems = numOfItems;
		}
	//the position of it	
	mePos = speech.transform.position;
	}
	float takeAway;
	
	// Update is called once per frame
	void Update () 
	{
		//if its is clicked or not, for the height of the bubble
		if (clicked == true)
		{
		takeAway = 15f;	
		}
		else
		{
		takeAway = 0f;
		}
		//lerping the posiont and transformation of the speech bubble
		if (mouseOver == true) 
		{
		
			speech.transform.position = new Vector3(this.transform.position.x, Mathf.Lerp(speech.transform.position.y, this.transform.position.y + above - takeAway, 0.2f), speech.transform.position.z);
			speech.transform.localScale = new Vector3 (Mathf.Lerp (speech.transform.localScale.x, 0.8f, 0.1f) ,Mathf.Lerp (speech.transform.localScale.y, 0.0f, 0.1f),Mathf.Lerp (speech.transform.localScale.z, 0.8f, 0.1f));
		}
		else if (clicked == false || mouseOver == false)
		{
			speech.transform.position = new Vector3(speech.transform.position.x, Mathf.Lerp(speech.transform.position.y, mePos.y , 0.2f), speech.transform.position.z);
			speech.transform.localScale = new Vector3 (Mathf.Lerp (speech.transform.localScale.x, 0.0f, 0.1f) ,Mathf.Lerp (speech.transform.localScale.y, 0.0f, 0.1f),Mathf.Lerp (speech.transform.localScale.z, 0.0f, 0.1f));		
		}
		
		if (clicked == true)
		{
			speech.transform.localScale = new Vector3 (Mathf.Lerp (speech.transform.localScale.x, 5, 0.1f) ,Mathf.Lerp (speech.transform.localScale.y, 5, 0.1f),Mathf.Lerp (speech.transform.localScale.z, 5, 0.1f));
			speech.transform.position = new Vector3 (speech.transform.position.x,speech.transform.position.y + 5,speech.transform.position.z);
		}
		
		if (mouseOver == true && Input.GetButtonDown("Fire1"))
		{
			clicked = true;
			
		}
		
		if (clicked == true && Input.GetButtonDown("Fire1") && mouseOver == false)
		{
			clicked = false;
		}
	
		
		
		


		

		
	}
	
	void OnMouseEnter ()
	{
		
		mouseOver = true;
	}
	
		void OnMouseExit ()
	{
		
		mouseOver = false;
	//	clicked = false;
	}
	
	
}
