using UnityEngine;
using System.Collections;

public class Mouse : MonoBehaviour {
	public Texture2D mouse;
	
	// Use this for initialization
	void Awake () 
	{
		DontDestroyOnLoad (this.transform.gameObject);
	}
	
	void Start () {
	Screen.showCursor = false;
	}
	
	// Update is called once per frame
	//sets the mouses image
	void OnGUI() {
		float mousex = Input.mousePosition.x;
		float mousey =Screen.height - (Input.mousePosition.y);
		GUI.DrawTexture(new Rect(mousex - 5,mousey - 5,166/3,287/3) , mouse );

	}
}
