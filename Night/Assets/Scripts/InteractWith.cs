using UnityEngine;
using System.Collections;

public class InteractWith : MonoBehaviour 
{
	
	[SerializeField] private string itemName;
	GameObject mother;
	[SerializeField] private GameObject baby;
	[SerializeField] GameObject[] objs;
	[SerializeField] private bool dies;
	[SerializeField] private bool kills;
	[SerializeField] private bool spawns;
	[SerializeField] private bool quest;
	[SerializeField] private bool sound;
	[SerializeField] private bool makeVis;
	bool hasrun;
	GameObject NPCQ;
	Vector3 mePos;
	GameObject	controller;
	[SerializeField] private Texture2D nomMouse;
	// Use this for initialization
	void Start () 
	{
	controller = GameObject.Find("Controller(Clone)");

	
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		
	}
	
	
	
	void OnTriggerStay(Collider trig)
	{
		//if the obect is over the stationary object
	
		if (Input.GetButtonUp("Fire1"))
			{	
				Debug.Log ("should run once");
				GameObject cam = GameObject.Find("Char");
	    		Movement camscript = cam.GetComponent<Movement>();
				camscript.interacting = false;
				
				Mouse Pic = controller.GetComponent<Mouse>();
	    		Pic.mouse = nomMouse;	
			
				mother = trig.gameObject;

				GameObject with = trig.gameObject;
			
        		Interactmove withscript = with.GetComponent<Interactmove>();
			
				withscript.hold = false;
				
				//if it has the correct tag
			if (hasrun == false)
			{
				if (trig.gameObject.tag == itemName)
					{
										
						if (quest == true)
							{
								Quest questGive = controller.GetComponent<Quest>();
								questGive.numInteracted = questGive.numInteracted + 1;
					
							}
						if (spawns == true)
							{
								Instantiate(baby, transform.position, transform.rotation);
							}
						if (makeVis == true)
							{
								for (int i = 0; i <= objs.Length; i++)
									{
										objs[i].SetActive(true);
									}
							}
						if (sound == true)
							{
								this.audio.PlayDelayed (0);
							}
						
						if (kills == true)
							{
								Destroy(trig.gameObject, 0.001f);
							}						
					
						if (dies == true)
							{
								Destroy (gameObject, 0.00001f);
							}
					
					hasrun = true;
						
					}

			}
		}
		


		}
			

	
						

		
				
	
}

