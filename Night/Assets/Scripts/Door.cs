﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {
	
	public GameObject arrow;
	bool enter;
	bool leaving;
	void start ()
	{
		arrow.transform.localScale = new Vector3(-1,-1,-1);		
	}
	void Update ()
	{
		if (enter == true)
		{
			arrow.transform.localScale = new Vector3(Mathf.Lerp(arrow.transform.localScale.x, 0.1f, 0.1f),Mathf.Lerp(arrow.transform.localScale.y, 0.1f, 0.1f),Mathf.Lerp(arrow.transform.localScale.z, 0.1f, 0.1f));
		}
		else
		{
			arrow.transform.localScale = new Vector3(Mathf.Lerp(arrow.transform.localScale.x, 0f, 0.1f),Mathf.Lerp(arrow.transform.localScale.y, 0f, 0.1f),Mathf.Lerp(arrow.transform.localScale.z, 0f, 0.1f));	
		}
	}
		
	
	void OnTriggerStay(Collider trig)
	{
		if (trig.gameObject.name == "Char")
		{
			enter = true;
		}
		
	}
	
	void OnTriggerExit(Collider trig)
	{
		enter = false;
	}
	
	

}
