﻿using UnityEngine;
using System.Collections;

public class Pyramid : MonoBehaviour {
	[SerializeField] public bool gameover;
	bool played = false;
	public float top;
	float origin;
	GameObject Char;
	GameObject Car;
	GameObject controller;
	[SerializeField] GameObject credits;
	[SerializeField] GameObject planet;
	[SerializeField] GameObject text;
	[SerializeField] Texture[] qAndA;
	bool mouseover;
	bool over;
	public bool complete;
	bool clicked;
	bool end = false;
	bool pyramid = false;
	[SerializeField] AudioClip endtheme;
	[SerializeField] AudioClip opening;
	[SerializeField] AudioClip closing;
	[SerializeField] GameObject arrow;

	// Use this for initialization
	void Start () {
	Char = GameObject.Find ("Char");
	Car = GameObject.Find ("car");
	controller =  GameObject.Find ("Controller(Clone)");
	
	top = Char.transform.position.y + top;
	origin = Char.transform.position.y;
	}
	
	// Update is called once per frame
	void Update () 
	{
		Quest  cont = controller.GetComponent<Quest>();
		Movement move = Char.GetComponent<Movement>();
		if (clicked == true)
		{
			Char.transform.position = new Vector3(Char.transform.position.x, (Mathf.Lerp (Char.transform.position.y, -75 , 0.01f)), Char.transform.position.z);
			Car.transform.position = new Vector3(Car.transform.position.x,-185f,Car.transform.position.z);
			move.go = 45f;
			
			
		}
		else if (clicked == false)
		{
			Char.transform.position = new Vector3(Char.transform.position.x, (Mathf.Lerp (Char.transform.position.y, -155 , 0.1f)), Char.transform.position.z);
			Car.transform.position = new Vector3(Car.transform.position.x,-185f,Car.transform.position.z);
			move.interacting = false;
		}
		if (clicked == true && cont.josies == true && cont.radio == true && cont.your == true && cont.mine == true && cont.forest == true && cont.diner == true)
		{
			complete = true;
		}
		if (clicked == true && Char.transform.position.x >= 44f && Char.transform.position.x <= 46f )
		{
			
			move.interacting = true;
		}
		if (over == true && clicked == true && complete == false)
		{
			text.renderer.material.color = Color.Lerp(text.renderer.material.color,Color.white,0.05f);
			
		}
		else
		{
			text.renderer.material.color = Color.Lerp(text.renderer.material.color,Color.clear,0.1f);
		}
		if (Input.GetButtonDown ("Fire1") && mouseover == false && complete == false)
		{
			clicked = false;
		}
		if (gameover == true)
		{
			Char.transform.position = new Vector3(Char.transform.position.x, (Mathf.Lerp (Char.transform.position.y, 0 , 0.01f)), Char.transform.position.z);
			planet.transform.position = new Vector3(planet.transform.position.x, (Mathf.Lerp (planet.transform.position.y, Char.transform.position.y , 0.005f)), planet.transform.position.z);
			
			planet.animation.Play ("planetgrow");
			Car.transform.position = new Vector3(Car.transform.position.x,-185f,Car.transform.position.z);
			
			credits.transform.position = new Vector3(credits.transform.position.x, Mathf.Lerp (credits.transform.position.y, 83f, 0.001f), credits.transform.position.z);
			if (credits.transform.position.y > 85)
			{
				Application.Quit();	
			}
		}
		
	}	
	void OnMouseOver()
	{
	
	mouseover =true;
	if (Input.GetButtonDown ("Fire1") && clicked == false)
		{
			clicked = true;
			if (pyramid == false)
			{
				controller.audio.clip = opening;
				controller.audio.Play ();
				pyramid = true;
			}
		}
	else if (Input.GetButtonDown ("Fire1") && clicked == true && complete == false)
		{
			clicked = false;
		}
		
	}
	void OnMouseExit()
	{
		mouseover = false;	
		
	}
	
	void OnTriggerEnter(Collider col)
	{
		over = true;
	}
	
	void OnTriggerExit(Collider col)
	{
		clicked	= false;
		over = false;
	}
	int num1;
	int num2;
	int num3;
	int num4;
	int num5;
	int num6;
	
	
	
	void OnGUI()
	{
		
		if (complete == true)
		{
	
				if (GUI.Button (new Rect ( Screen.width/2 - 450, Screen.height/2, 100, 100), qAndA[num1], GUIStyle.none))
				{
				Debug.Log (num1);
					num1 = num1 +1;
					if (num1 > 5)
					{
						num1 =0;	
					}
				}
				if (GUI.Button (new Rect ( Screen.width/2-300, Screen.height/2, 100, 100), qAndA[num2], GUIStyle.none))
				{
				Debug.Log (num2);
					num2 = num2 +1;
					if (num2 > 5)
					{
						num2 =0;	
					}
				}
				if (GUI.Button (new Rect (Screen.width/2 - 150, Screen.height/2, 100, 100), qAndA[num3], GUIStyle.none))
				{
				Debug.Log (num3);
					num3 = num3 +1;
					if (num3 > 5)
					{
						num3 =0;	
					}
				}
				if (GUI.Button (new Rect (40+ Screen.width/2, Screen.height/2, 100, 100), qAndA[num4], GUIStyle.none))
				{
				Debug.Log (num4);
					num4 = num4 +1;
					if (num4 > 5)
					{
						num4 =0;	
					}
				}
				if (GUI.Button (new Rect (190+ Screen.width/2, Screen.height/2, 100, 100), qAndA[num5], GUIStyle.none))
				{
				Debug.Log (num5);
					num5 = num5 +1;
					if (num5 > 5)
					{
						num5 =0;	
					}
				}
				if (GUI.Button (new Rect (340+ Screen.width/2, Screen.height/2, 100, 100), qAndA[num6], GUIStyle.none))
				{
				Debug.Log (num6);
					num6 = num6 +1;
					if (num6 > 5)
					{
						num6 =0;	
					}
				}
				if (num1 == 4 && num2 == 1 && num3 == 0 && num4 == 5 && num5 == 2 && num6 == 3)
				{
					gameover = true;
				}
		}
	}
}
