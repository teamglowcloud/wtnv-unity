using UnityEngine;
using System.Collections;

public class Interactmove : MonoBehaviour {
	public bool hold = false;
	public Vector3  home;
	public Texture2D overMouse;
	public Texture2D nomMouse;
	GameObject controller;
	GameObject cam;
	// Use this for initialization
	void Start () 
	{
	//gets where the item exists and should return to
	controller = GameObject.Find("Controller(Clone)");
	home  = this.transform.position;
	
	controller = GameObject.Find("Controller(Clone)");
	Quest comp = controller.GetComponent<Quest>();
		//persistance
		if (comp.josies == true && Application.loadedLevelName == "Josies")
		{
			Destroy (gameObject);
		}
		
		if (comp.radio == true && Application.loadedLevelName == "Radio-Toilet")
		{
			Destroy (gameObject);
		}
		
		if (comp.your == true && Application.loadedLevelName == "Your-Trailer")
		{
			Destroy (gameObject);
		}
		
		if (comp.mine == true && Application.loadedLevelName == "Mine")
		{
			Destroy (gameObject);
		}
		
		if (comp.forest == true && Application.loadedLevelName == "Forest")
		{
			Destroy (gameObject);
		}
		
		if (comp.diner == true && Application.loadedLevelName == "Diner")
		{
			Destroy (gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
	Debug.Log(hold);
		//finds the camera, and then finds the position of the mouse in the 3D world
	if (hold == true)
		{

		cam = GameObject.Find("Char");
        Movement camscript = cam.GetComponent<Movement>();
		camscript.interacting = true;
		//transforms the position of the item to the position of the mouse	
		this.transform.position = new Vector3(camscript.worldPos.x,camscript.worldPos.y, this.transform.position.z) ;
			
		}
	else
		{

		}
			
	}
	
	void OnMouseOver () 
	{

		//will allow for the itme to me "picked up" if the mouse is over, and put down
		
		if (Input.GetButtonDown("Fire1"))
		{
			cam = GameObject.Find("Char");
	       	Movement camscript = cam.GetComponent<Movement>();
			
			
			Debug.Log ("TURNGON ON");
			hold = true;
			
		}
		else if (Input.GetButtonUp("Fire1") && hold == true )
		{
			this.transform.position = home;
			hold = false;
			
			cam = GameObject.Find("Char");
	    	Movement camscript = cam.GetComponent<Movement>();
			camscript.interacting = false;
		}
		Mouse Pic = controller.GetComponent<Mouse>();
	    Pic.mouse = overMouse;
		
	}
	
	void OnMouseEnter ()
	{
		
		Mouse Pic = controller.GetComponent<Mouse>();
	    Pic.mouse = overMouse;
	}
	
	void OnMouseExit ()
	{

		
		Mouse Pic = controller.GetComponent<Mouse>();
	    Pic.mouse = nomMouse;	
	}
}
